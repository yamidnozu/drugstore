import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';

export class RestBase {

    private url: string;

    constructor(
        protected http: HttpClient,
        private resource: string
    ) {
        this.url = `${environment.serverUrl}/${resource}`;
    }

    public getAll<T>(): Observable<T> {
        return this.http.get<T>(this.url);
    }

    public getSingle<T>(id: number | string): Observable<T> {
        return this.http.get<T>(`${this.url}/${id}`);
    }

    public get<T>(id: number | string): Observable<T> {
        return this.http.get<T>(`${this.url}/${id}`);
    }

    public getQueryParams<T>(params: string): Observable<T> {
        return this.http.get<T>(`${this.url}?${params}`);
    }

    public add<T>(item: T): Observable<T> {
        return this.http.post<T>(this.url, item);
    }

    public update<T>(id: number, itemToUpdate: any): Observable<T> {
        return this.http
            .put<T>(`${this.url}/${id}`, itemToUpdate);
    }

    public updateQueryParams<T>(params: string, itemToUpdate: any): Observable<T> {
        return this.http
            .put<T>(`${this.url}?${params}`, itemToUpdate);
    }

    public updateMultipleInBody<T>(itemToUpdate: any): Observable<T> {
        return this.http
            .put<T>(`${this.url}`, itemToUpdate);
    }

    public delete<T>(id: number): Observable<T> {
        return this.http.delete<T>(`${this.url}/${id}`);
    }

    public deleteByParam<T>(params: string): Observable<T> {
        return this.http.delete<T>(`${this.url}?${params}`);
    }
}
