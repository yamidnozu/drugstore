import { KeycloakService } from './keycloak.service';

export function initializer(keycloak: KeycloakService): () => Promise<any> {
    return (): Promise<any> => {
        return keycloak.init();
    };
}
