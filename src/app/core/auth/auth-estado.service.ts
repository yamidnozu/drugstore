import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { AutenticacionMode } from '@core/auth/auth.model';

import { KeycloakInstance, KeycloakTokenParsed } from 'keycloak-js';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AutEstadoService {

  private readonly autSubject$ = new BehaviorSubject<AutenticacionMode>({ estaLogeado: false });
  readonly autenticacion$ = this.autSubject$.asObservable();
  private readonly tokenSubject$ = new BehaviorSubject<string>('');
  readonly token$ = this.tokenSubject$.asObservable();

  constructor() { }

  get autenticacion(): AutenticacionMode {
    return this.autSubject$.getValue();
  }

  set autenticacion(val: AutenticacionMode) {
    this.autSubject$.next(val);
  }

  get token(): string {
    return this.tokenSubject$.getValue();
  }

  set token(val: string) {
    this.tokenSubject$.next(val);
  }

  get tokenParsed(): KeycloakTokenParsed {
    if (!this.autenticacion.estaLogeado) {
      return {};
    }
    return this.autenticacion.instancia.tokenParsed;
  }

  autenticacionExitosa(instancia: KeycloakInstance) {
    this.autenticacion = { estaLogeado: true, instancia };
    this.token = instancia.token;
  }

  cerrarSesion() {
    this.autenticacion = { estaLogeado: false };
  }

  updateToken(): void {
    if (this.token !== '') {
      this.autenticacion.instancia.updateToken(environment.keycloak.expiredTime)
        .success((refreshed) => {
          if (refreshed) {
            this.tokenSubject$.next(this.autenticacion.instancia.token);
          }
        })
        .error(() => {
          this.tokenSubject$.error('Error refrescando token');
        });
    } else {
      console.error('No se puede refrescar el token');
    }
  }

}
