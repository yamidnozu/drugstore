import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AutEstadoService } from './auth-estado.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService {

  constructor(private autEstService: AutEstadoService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = this.autEstService.token;
    let request = req;
    if (token) {
      request = req.clone({ setHeaders: { authorization: `Bearer ${token}` } });
    }
    return next.handle(request);
  }
}
