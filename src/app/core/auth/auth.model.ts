import { KeycloakInstance } from 'keycloak-js';

export interface AutenticacionMode {
    estaLogeado: boolean;
    instancia?: KeycloakInstance;
}

