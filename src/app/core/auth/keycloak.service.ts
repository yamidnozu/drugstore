import { Injectable } from '@angular/core';
import { AutEstadoService } from './auth-estado.service';
import { KeycloakInstance } from 'keycloak-js';
import { environment } from '@environments/environment';
import * as Keycloak_ from 'keycloak-js';
export declare const Keycloak: typeof Keycloak_;

@Injectable({
  providedIn: 'root'
})
export class KeycloakService {
  private authKeycloak: KeycloakInstance;
  // /realms/demo/protocol/openid-connect/logout?redirect_uri=/angular2-product/index.html
  constructor(
    private autEstService: AutEstadoService
  ) { }

  init(): Promise<any> {
    this.authKeycloak = Keycloak(environment.keycloak); // Configurar con valores del environment
    return new Promise((resolve, reject) => {
      this.authKeycloak
        .init({ onLoad: 'login-required' })
        .success((res) => {
          console.log('respuesta: ' + res);
          this.registrarEventoExpiracion();
          this.autEstService.autenticacionExitosa(this.authKeycloak);
          this.autEstService.updateToken();
          resolve();
        })
        .error(() => {
          reject();
        });
    });
  }

  logout() {
    this.autEstService.cerrarSesion();
    window.location.href =
      `
      ${environment.keycloak.url}/realms/${environment.keycloak.realm}` +
      `/protocol/openid-connect/logout?redirect_uri=${environment.keycloak.homeUri}`; // logout url desde enviroments;
  }

  private registrarEventoExpiracion() {
    this.authKeycloak.onTokenExpired = () => {
      this.openDialog();
    };
  }

  private openDialog() {
    // Abrir ventana, luego de cerrarla invocar a this.logout();
    this.logout();
  }
}
