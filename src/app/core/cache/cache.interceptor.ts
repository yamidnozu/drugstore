import { Injectable } from "@angular/core";
import {
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
    HttpResponse
} from "@angular/common/http";

import { CacheRegistrationService } from "@core/cache";
import { Observable, of } from 'rxjs';
import { tap, share } from 'rxjs/operators';

import "rxjs/add/operator/do";

@Injectable({
    providedIn: 'root'
})
export class UriCacheInterceptor implements HttpInterceptor {
    private cache = new Map<string, any>();

    constructor(
        private cacheRegistrationService: CacheRegistrationService
    ) { }

    public intercept(httpRequest: HttpRequest<any>, handler: HttpHandler) {
        // Si no es cacheable
        // 1. Si no es una petición GET
        // 2. Si la petición no ha sido cacheada
        // -> Continua con la petición
        if (httpRequest.method !== "GET" ||
            !this.cacheRegistrationService.addedToCache(httpRequest.url)) {
            return handler.handle(httpRequest);
        }

        // Si se pide que se restablezca los datos ya cacheado
        if (httpRequest.headers.get("reset-cache")) {
            this.cache.delete(httpRequest.urlWithParams);
        }

        // Revisa si ya anteriormente ha sido cacheado
        const lastResponse = this.cache.get(httpRequest.urlWithParams);
        if (lastResponse) {
            // En caso de que en paralelo haya una petición a la url
            // retorna la petición que ya esta en proceso
            // de otra forma retorna la respuesta queya está cacheada
            return (lastResponse instanceof Observable)
                ? lastResponse : of(lastResponse.clone());
        }

        // Si la solicitud es su primera vez
        // entonces se hace la petición y se almacena la respuesta
        const requestHandle = handler.handle(httpRequest).pipe(
            tap(stateEvent => {
                if (stateEvent instanceof HttpResponse) {
                    this.cache.set(
                        httpRequest.urlWithParams,
                        stateEvent.clone()
                    );
                }
            }), share());

        // Mientras tanto se guarda en cache la solicitud observable para manejar solicitudes paralelas
        this.cache.set(httpRequest.urlWithParams, requestHandle);

        return requestHandle;
    }
}