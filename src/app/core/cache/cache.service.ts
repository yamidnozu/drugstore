import { Injectable } from "@angular/core";


/**
 * @service Solamente agrega una lista de url de apis que se estarán
 * cacheando dentro de la misma sesión. ( Temporal en ram )
 * Es posible modificarla a que se almacene en localstorage o Sesionstorage
 */

@Injectable({
    providedIn: 'root'
})
export class CacheRegistrationService {
    private cacheados = [];

    public addedToCache(inUrlApi: string) {
        return this.cacheados.indexOf(inUrlApi) > -1;
    }

    public addToCache(inUrlApi: string) {
        // Revisa si ha sido agregado a la cache
        if (!this.addedToCache(inUrlApi)) {
            this.cacheados.push(inUrlApi);
        }
    }
}