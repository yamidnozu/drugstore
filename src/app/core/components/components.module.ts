import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SidebarItemComponent } from './sidebar-item/sidebar-item.component';
import { TopbarComponent } from './topbar/topbar.component';
import { ResizableComponent } from './resizable/resizable.component';
import { CardComponent } from './card/card.component';
import { IndicadorComponent } from './indicador/indicador.component';
import { DataComponent } from './data/data.component';



@NgModule({
  declarations: [
    SidebarComponent,
    SidebarItemComponent,
    TopbarComponent,
    ResizableComponent,
    CardComponent,
    IndicadorComponent,
    DataComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
  ],
  exports: [
    SidebarComponent,
    SidebarItemComponent,
    TopbarComponent,
    ResizableComponent,
    CardComponent,
    IndicadorComponent,
    DataComponent,
  ],
})
export class ComponentsModule { }
