import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ds-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent implements OnInit {

  @Input() title = null;
  @Input() number = null;
  @Input() icon = null;
  constructor() { }

  ngOnInit(): void {
  }

}
