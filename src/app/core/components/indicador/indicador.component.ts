import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ds-indicador',
  templateUrl: './indicador.component.html',
  styleUrls: ['./indicador.component.scss']
})
export class IndicadorComponent implements OnInit {
  @Input() icon = null;
  @Input() text = null;
  @Input() data: string = null;
  constructor() { }

  ngOnInit(): void {
  }

}
