import { Component, ElementRef, HostBinding, Inject, Renderer2, ViewEncapsulation, AfterViewInit, EventEmitter, Output, Input } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
    selector: 'ds-splitter',
    templateUrl: './resizable.component.html',
    styleUrls: ['./resizable.component.scss']
})
export class ResizableComponent implements AfterViewInit {
    /** Flag that indicates when dragging is occuring */
    @HostBinding('class.is-dragging') private dragging: boolean = false;
    // @HostBinding('style.width.px') private width: number;

    /** Emit event when resizing so that other components can react */
    @Output() public resize: EventEmitter<void> = new EventEmitter();

    @Input() public ref?: ElementRef;

    /** The x position of the mouse when we start dragging. Relative to the screen */
    private startScreenX: number = 0;

    /** The width of the host element when we started dragging. */
    private startWidth: number = 0;

    constructor(private el: ElementRef, private renderer: Renderer2, @Inject(DOCUMENT) private document: Document) { }

    ngAfterViewInit() {
        setTimeout(() => {
            // this.width = this.el.nativeElement.offsetWidth;
        })
    }

    startDrag(event: MouseEvent) {
        this.dragging = true;
        this.startScreenX = event.screenX;

        const el = this.ref ? this.ref : this.el.nativeElement;
        this.startWidth = el.offsetWidth;

        this.document.addEventListener('mouseup', this.stopDrag.bind(this));
        this.document.addEventListener('mousemove', this.drag.bind(this));
    }

    stopDrag() {
        this.dragging = false;
        this.document.removeEventListener('mouseup', this.stopDrag);
        this.document.removeEventListener('mousemove', this.drag);
    }

    drag(event: MouseEvent) {
        if (this.dragging) {
            const newWidth = this.startWidth + (event.screenX - this.startScreenX);
            // this.width = newWidth;
            if (this.ref) {
                this.renderer.setStyle(this.ref, 'width', newWidth + 'px');
            } else {
                this.renderer.setStyle(this.el.nativeElement, 'width', newWidth + 'px');
            }

            this.resize.emit();
        }
    }
}