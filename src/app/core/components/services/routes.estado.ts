import { Store } from '@app/core/store/store';
import { Injectable } from '@angular/core';
import { map, distinctUntilChanged } from 'rxjs/operators';
export interface RoutesEstadoModel {
  // Agregar miembros de la interfaz
  menu: any[]
}
const estadoDefault: RoutesEstadoModel = {
  // Agregar valores a miembros según su interfaz
  menu: [
    { view: 'Principal', path: 'home', icon: 'fas fa-laptop-house' },
    { view: 'Salir', path: 'home', icon: 'fas fa-sign-out-alt' },
  ]
};
@Injectable({
  providedIn: 'root'
})
export class RoutesEstado extends Store<RoutesEstadoModel> {
  readonly menu$ = this.state$.
    pipe(
      map(el => el.menu),
      distinctUntilChanged()
    );

  set menu(payload: any) {
    this.setState({ ...this.state, menu: payload });
  }

  get menu() {
    return this.state.menu;
  }

  constructor() {
    super(estadoDefault);
  }

}