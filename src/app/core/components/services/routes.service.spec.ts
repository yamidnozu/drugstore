import { TestBed } from '@angular/core/testing';

import { RoutesEstado } from './routes.estado';

describe('RoutesEstado', () => {
  let service: RoutesEstado;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RoutesEstado);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
