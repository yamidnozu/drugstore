import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ds-sidebar-item',
  templateUrl: './sidebar-item.component.html',
  styleUrls: ['./sidebar-item.component.scss']
})
export class SidebarItemComponent implements OnInit {
  @Input() shadow: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

}
