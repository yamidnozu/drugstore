import { Component, OnInit, ElementRef } from '@angular/core';
import { RoutesEstado } from '../services/routes.estado';

@Component({
  selector: 'ds-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public expanded: boolean = false;

  constructor(
    public routesEstado: RoutesEstado,
    public elementRef: ElementRef,
  ) {

  }

  ngOnInit() {

  }

  public getIcon(item: { view, value, icon }) {
    return 'action-bar-item-icon ' + item.icon;
  }


}
