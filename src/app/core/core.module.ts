import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CacheRegistrationService, UriCacheInterceptor } from '@core/cache';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ContentComponent } from './layouts/content/content.component';
import { ComponentsModule } from './components/components.module';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        ContentComponent,
    ],
    imports: [
        CommonModule,
        ComponentsModule,
        RouterModule,
    ], exports: [
    ],
    providers: [
        CacheRegistrationService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: UriCacheInterceptor,
            multi: true
        }
    ]
})
export class CoreModule { }

