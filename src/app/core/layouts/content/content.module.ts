import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContentRoutingModule } from './content-routing.module';
import { ContentComponent } from './content.component';
import { ComponentsModule } from '@app/core/components/components.module';


@NgModule({
  declarations: [
    ContentComponent,
  ],
  imports: [
    CommonModule,
    ContentRoutingModule,
    ComponentsModule,
  ], exports: [
    
  ]
})
export class ContentModule { }
