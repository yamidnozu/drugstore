import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';

@Injectable({
    providedIn: 'root'
})
export class StorageService {
    storage: Storage;

    constructor() {
        if (environment.localStorage) {
            this.storage = localStorage;
        } else {
            this.storage = sessionStorage;
        }
    }

    getData(key: string): any {
        return this.storage.getItem(key);
    }

    saveData(key: string, data: any) {
        this.storage.setItem(key, JSON.stringify(data));
    }
}
