export class Conversiones {
    /**
     * Métodos y funciones que procesan información de entrada y tienen una salida a un tipo distinto
     */

    /**
    * @param inObjeto inObjeto { campo1: valor1, campo2: valor2,  ...  }
    * @description Retorna una cadena de parámetros 'campo1=valor1&camop2:valor2& .... &campoN=valorN}
    */
    public static convertObjectToParams(inObjeto: any): string {
        let resultado = [];
        let outParams = '';
        Object.keys(inObjeto).forEach(campo => {
            if (inObjeto[campo] !== null && inObjeto[campo] !== undefined) {
                resultado = [...resultado, { [campo]: inObjeto[campo] }];
            }
        });
        resultado.map((el, i, arreglo) =>
            outParams += (i < arreglo.length - 1) ?
                `${Object.keys(el)}=${Object.values(el).toString().trim()}&` :
                `${Object.keys(el)}=${Object.values(el).toString().trim()}`
        );

        return outParams;
    }

    /**
     * Funciones con fechas y horas
     */
    static dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/;
    static reviverDateToJson = (key, value) =>
        typeof value === 'string' && Conversiones.dateFormat.test(value) ? new Date(value) : value;

    // Permite convertir de campos que tienen fechas en tipo string
    // a campos de tipo Date
    // Ejemplo de uso
    // const text = '{ "date": "2016-04-26T18:09:16Z" }';
    // const obj = reviverDateToJson(text);
    // console.log(typeof obj.date); Salida: { date: Date }
    /** 
     * @description Permite convertir de campos que tienen fechas en tipo string a campos de tipo Date
     * @example const text = '{ "date": "2016-04-26T18:09:16Z" }';
     *          const obj = reviverDateToJson(text);
     *          console.log(typeof obj.date); 
     *          Salida: { date: Date }
     */
    static parseJsonObject = (inObject: any): any => {
        return JSON.parse(inObject, Conversiones.reviverDateToJson);
    };
}