import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ds-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  data = [
    { title: 'Claro', number: '$ 12.500.000', icon: 'fas fa-chart-bar', indicador: { icon: 'fa fa-arrow-up', text: 'A la alza', data: '35%' } },
    { title: 'Movistar', number: '$ 30.500.000', icon: 'fas fa-chart-bar', indicador: { icon: 'fa fa-arrow-up', text: 'Proyección a corto plazo', data: '100%' } },
    { title: 'Electricaribe', number: '$ 2.500.000', icon: 'fas fa-chart-bar', indicador: { icon: 'fa fa-arrow-down', text: 'Pérdidas leves', data: '5%' } },
    { title: 'Ecopetrol', number: '$ 5.500.000', icon: 'fas fa-chart-bar', indicador: { icon: 'fa fa-arrow-down', text: 'Recuperandose', data: '15%' } },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
