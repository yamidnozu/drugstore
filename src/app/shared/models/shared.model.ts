/**
 * Modelos correspondientes a ventanas de tipo información
 */
export interface InfoModelSh {
    duracion?: number;
    mensaje: string;
    tipo?: InfoTipoSh;
}

export enum InfoTipoSh {
    INFO,
    ALERTA,
    ERROR
}

export enum InfoDuracionSh {
    CORTO = 1000,
    MEDIO = 5000,
    LARGO = 20000
}

/**
 * Modelos correspondientes a ventanas de tipo diálogos simples
 */
export interface DialogoSimpleModelSh {
    opcionAceptar?: string;
    opcionCancelar?: string;
    titulo?: string;
    contenido?: string;
    color?: ColorDialogoEnumSh;
    tipo?: TipoDialogoEnumSh;
}

export enum ColorDialogoEnumSh {
    advertencia = 'advertencia',
    info = 'info',
    error = 'error',
}

export enum TipoDialogoEnumSh {
    advertencia = 'advertencia',
    info = 'info',
    error = 'error',
}
