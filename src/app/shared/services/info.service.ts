import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { skip } from 'rxjs/operators';
import { InfoModelSh } from '../models/shared.model';

@Injectable({
    providedIn: 'root'
})
export class InfoServiceSh {

    private readonly infoSubject$ = new BehaviorSubject<InfoModelSh>({ mensaje: '' });
    readonly info$ = this.infoSubject$.asObservable().pipe(skip(1));

    constructor() { this.info$.subscribe(); }

    mostrarMensaje(msj: InfoModelSh) {
        this.infoSubject$.next(msj);
    }
}
