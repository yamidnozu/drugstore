import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { InfoServiceSh } from '@shared/services';
import { CommonModule } from '@angular/common';


@NgModule({
    declarations: [

    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,

    ], exports: [
        ReactiveFormsModule,
        FormsModule,

    ], entryComponents: [

    ],
})
export class SharedModule {
    static forRoot() {
        return {
            ngModule: SharedModule,
            providers: [
                InfoServiceSh,
            ]
        };
    }
}
