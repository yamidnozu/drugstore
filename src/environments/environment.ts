const keycloakConfig = {
  url: 'http://35.238.54.102:8080/auth',
  realm: 'drugstore',
  clientId: 'drugstore-front',
  homeUri: 'http://localhost',
  expiredTime: 900
};


export const environment = {
  production: false,
  keycloak: keycloakConfig,
  serverUrl: 'http://10.34.87.24:8081/api',
  localStorage: true
};
